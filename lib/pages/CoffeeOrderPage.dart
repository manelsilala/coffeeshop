import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app/widgets//date_time_picker_widget2.dart';

class CoffeeOrderPage extends StatefulWidget {
  @override
  _CoffeeOrderPageState createState() => _CoffeeOrderPageState();
}

class _CoffeeOrderPageState extends State<CoffeeOrderPage> {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF0EAE7),
      appBar: AppBar(
        backgroundColor: Color(0xFFF0EAE7),
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xFF3a3737),
          ), // fleche thez lel page coffedetailpage
          onPressed: () => Navigator.of(context).pop(), // bech el fleche te5dem
        ),
        title: Center(
          //titre du page
          child: Text(
            "Item Carts",
            style: TextStyle(
                color: Color(0xFF000000),
                fontWeight: FontWeight.w600,
                fontSize: 18),
          ),
        ),
        brightness: Brightness.light,
        actions: <Widget>[
          CartIconWithBadge(),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),

          /// padding
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 5),
                child: Text(
                  "Your Coffee Cart",
                  style: TextStyle(
                      fontSize: 17,
                      color: Color(0xFF000000),
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              CartItem(
                  productName: "Americano",
                  productPrice: "\3.000 DT",
                  productImage: "americano",
                  productCartQuantity: "1"),
              SizedBox(
                height: 2,
              ),
              CartItem(
                  productName: "cafe creme",
                  productPrice: "\6.000 DT",
                  productImage: "cafe_creme",
                  productCartQuantity: "1"),
              SizedBox(
                height: 2,
              ), //SizedBox
              PromoCodeWidget(),
              SizedBox(
                height: 5,
              ), //SizedBox
              TotalCalculationWidget(),
              SizedBox(
                height: 2,
              ), //SizedBox

              PickUpTime(),
              SizedBox(
                height: 2,
              ), //SizedBox
              Container(
                padding: EdgeInsets.only(left: 5),
                child: Text(
                  "Payment Method",
                  style: TextStyle(
                      fontSize: 17,
                      color: Color(0xFF000000),
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(
                height: 2,
              ), //SizedBox
              PaymentMethodWidget(),
            ],
          ),
        ),
      ),
      //bottomNavigationBar: BottomNavBarWidget(),
    );
  }
}

class PaymentMethodWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: 60,
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Color(0xFFfae3e2).withOpacity(0.1),
          spreadRadius: 1,
          blurRadius: 1,
          offset: Offset(0, 1),
        ),
      ]),
      child: Card(
        color: Color(0xFF3EFCEAD),
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: const BorderRadius.all(
            Radius.circular(5.0),
          ),
        ),
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(left: 10, right: 30, top: 10, bottom: 10),
          child: Row(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: Image.asset(
                  "assets/images/ic_credit_card.png",
                  width: 50,
                  height: 50,
                ),
              ),
              Text(
                "Credit/Debit Card",
                style: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF3a3a3b),
                    fontWeight: FontWeight.w400),
                textAlign: TextAlign.left,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class TotalCalculationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: 150,
      child: Card(
        color: Color(0xffEFCEAD),
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: const BorderRadius.all(
            Radius.circular(5.0),
          ),
        ),
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(left: 25, right: 30, top: 10, bottom: 10),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Americano",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color(0xFF000000),
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    "\3.000 DT",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color(0xFF000000),
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.left,
                  )
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "cafe creme",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color(0xFF000000),
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    "\6.000 DT",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color(0xFF000000),
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.left,
                  )
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Total",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color(0xFF000000),
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    "\9.000 DT",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color(0xFF000000),
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.left,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CartIconWithBadge extends StatelessWidget {
  final int counter = 2;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        IconButton(
            icon: Icon(
              Icons.business_center,
              color: Color(0xFF3EFCEAD),
              size: 30.0,
            ),
            onPressed: () {}),
        counter != 0
            ? Positioned(
                right: 10,
                top: 5,
                child: Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 15,
                    minHeight: 12,
                  ),
                  child: Text(
                    '$counter',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 15,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            : Container()
      ],
    );
  }
}

class PromoCodeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.only(left: 3, right: 3),
        child: TextFormField(
          decoration: InputDecoration(
              fillColor: Color(0xffEFCEAD),
              hintStyle: TextStyle(color: Color(0xFF7A4009)),
              hintText: 'Add Your Promo Code',
              hoverColor: Colors.black,
              filled: true,
              suffixIcon: IconButton(
                  icon: Icon(
                    Icons.local_offer,
                    color: Color(0xFF7A4009),
                  ),
                  onPressed: () {
                    debugPrint('write promo code here');
                  })),
        ),
      ),
    );
  }
}

class PickUpTime extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          width: 500,
          padding: EdgeInsets.only(left: 0, right: 0),
          child: DateTimePickerWidget2()),
    );
  }
}

class CartItem extends StatelessWidget {
  final String productName;
  final String productPrice;
  final String productImage;
  final String productCartQuantity;

  CartItem({
    Key key,
    @required this.productName,
    @required this.productPrice,
    @required this.productImage,
    @required this.productCartQuantity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 112,
      child: Card(
          color: Color(0xFFEFCEAD),
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 2, right: 2, top: 5, bottom: 0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Center(
                        child: Image.asset(
                      "assets/images/$productImage.png",
                      width: 110,
                      height: 100,
                    )),
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 1,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "$productName",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Color(0xFF000000),
                                    fontWeight: FontWeight.w400),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              child: Text(
                                "$productPrice",
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Color(0xFF000000),
                                    fontWeight: FontWeight.w400),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 40,
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Image.asset(
                            "assets/images/ic_delete.png",
                            width: 25,
                            height: 25,
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20),
                      alignment: Alignment(0, 1),
                      child: AddToCartMenu(2),
                    )
                  ],
                )
              ],
            ),
          )),
    );
  }
}

class AddToCartMenu extends StatelessWidget {
  final int productCounter;

  AddToCartMenu(this.productCounter);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.remove),
            color: Color(0xFF895B4A),
            iconSize: 12,
          ),
          InkWell(
            child: Container(
              width: 94.0,
              height: 20.0,
              decoration: BoxDecoration(
                color: Color(0xFF895B4A),
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Center(
                child: Text(
                  'Add To $productCounter',
                  style: new TextStyle(
                      fontSize: 12.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w300),
                ),
              ),
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.add),
            color: Color(0xFF895B4A),
            iconSize: 12,
          ),
        ],
      ),
    );
  }
}
