import 'package:flutter/material.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/pages/SignUpPage.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String defaultFontFamily = 'Alef.ttf';
    double defaultFontSize = 20;

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 20, right: 20, top: 100, bottom: 30),
        width: double.infinity,
        height: double.infinity,
        color: Color(0xFFDACCBA),
        child: Column(
          children: <Widget>[
            Flexible(
              flex: 2,
              // child: Align(
              // alignment: Alignment.center,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text(
                      "Welcome to cup'O Coffee",
                      style: TextStyle(
                        color: Color(0xFF9F8C86),
                        fontFamily: defaultFontFamily,
                        fontSize: defaultFontSize,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //),
            Flexible(
              flex: 10,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 300,
                    height: 320,
                    alignment: Alignment.center,
                    child: Image.asset('assets/images/menus/welcome.png'),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Flexible(
              child: InkWell(
                child: WelcomeButtonWidget(),
                onTap: () {
                  Navigator.push(context, ScaleRoute(page: SignUpPage()));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class WelcomeButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: new BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          color: Color(0xFF895B4A)),
      child: MaterialButton(
          highlightColor: Colors.transparent,
          splashColor: Color(0xFF895B4A),
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 60.0),
            child: Text(
              "Continue",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontFamily: "WorkSansBold"),
            ),
          ),
          onPressed: () {
            Navigator.push(context, ScaleRoute(page: SignUpPage()));
          }),
    );
  }
}
