import 'package:flutter/material.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/pages/CoffeeOrderPagebackend.dart';
import 'package:flutter_app/widgets/topproductmodel.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app/widgets/productsmodal.dart';

class CoffeeDetailsPage2 extends StatefulWidget {
  final TopProducts product;

  const CoffeeDetailsPage2({@required this.product});

  @override
  _CoffeeDetailsPageState2 createState() => _CoffeeDetailsPageState2();
}

class _CoffeeDetailsPageState2 extends State<CoffeeDetailsPage2> {
  final _key = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Color(0xFFF0EAE7),
        appBar: AppBar(
          backgroundColor: Color(0xFFF0EAE7),
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Color(0xFF3a3737),
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.shopping_cart_rounded,
                  color: Color(0xFF3a3737),
                ),
                onPressed: () {
                  //Navigator.push(context, ScaleRoute(page: CoffeeOrderPage(product: widget.product)));
                })
          ],
        ),
        body: Container(
          padding: EdgeInsets.only(
            left: 15,
            right: 15,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Center(child: Image.network("${widget.product.image}")),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(0),
              ),
              CoffeeTitleWidget(
                  productName: (widget.product.name) ?? 'oo',
                  productPrice: (widget.product.price.toString()) ?? '00'),
              Container(
                height: 50.0,
                width: 500,
                decoration: new BoxDecoration(
                  color: Color(0xFFEFCEAD),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Text(
                  'ingredients',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      backgroundColor: Color(0xFFEFCEAD),
                      fontSize: 30,
                      color: Color(0xFF895B4A)),
                ),
              ),
              Container(
                decoration: new BoxDecoration(
                  color: Color(0xFFEFCEAD),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                height: 180,
                child: TabBarView(
                  children: [
                    Container(
                        height: 80,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            Coffeeingredients(
                                name: "bean", imageUrl: "bean", slug: ""),
                            Coffeeingredients(
                                name: "sugar", imageUrl: "sugar", slug: ""),
                            Coffeeingredients(
                                name: "ice", imageUrl: "cube", slug: ""),
                            Coffeeingredients(
                                name: "syrup", imageUrl: "syrup", slug: ""),
                            Coffeeingredients(
                                name: "water", imageUrl: "water", slug: ""),
                          ],
                        )),
                    Container(
                      color: Color(0xFFEFCEAD),
                    ), // class name
                  ],
                ),
              ),
              AddToCartMenu(),
            ],
          ),
        ),
      ),
    );
  }
}

class CoffeeTitleWidget extends StatelessWidget {
  String productName;
  String productPrice;
  CoffeeTitleWidget({
    Key key,
    @required this.productName,
    @required this.productPrice,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              productName,
              style: TextStyle(
                  fontSize: 25,
                  color: Color(0xFF3a3a3b),
                  fontWeight: FontWeight.w500),
            ),
            Text(
              productPrice,
              style: TextStyle(
                  fontSize: 25,
                  color: Color(0xFF3a3a3b),
                  fontWeight: FontWeight.w500),
            ),
          ],
        ),
      ],
    );
  }
}

class AddToCartMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.remove),
            color: Color(0xFF895B4A),
            iconSize: 20,
          ),
          InkWell(
            onTap: () {
              //Navigator.push(context, ScaleRoute(page: CoffeeOrderPage()));
            },
            child: Container(
              width: 260.0,
              height: 60.0,
              decoration: new BoxDecoration(
                color: Color(0xFF895B4A),
                border: Border.all(color: Color(0xFF895B4A), width: 2.0),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Center(
                child: Text(
                  'Add To Bag',
                  style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.add),
            color: Color(0xFF895B4A),
            iconSize: 25,
          ),
        ],
      ),
    );
  }
}

class Coffeeingredients extends StatelessWidget {
  String name;
  String imageUrl;
  String slug;

  Coffeeingredients(
      {Key key,
      @required this.name,
      @required this.imageUrl,
      @required this.slug})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Column(children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 0, right: 15, top: 15, bottom: 0),
          decoration: new BoxDecoration(boxShadow: []),
          child: Card(
            color: Color(0xFFEFCEAD),
            shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.all(
                Radius.circular(0.0),
              ),
            ),
            child: Container(
              width: 80,
              height: 80,
              child: Center(
                child: Image.asset(
                  'assets/images/ingredients/' + imageUrl + ".png",
                  width: 60,
                  height: 60,
                ),
              ),
            ),
            elevation: 0,
          ),
        ),
        CounterView(),
      ]),
    );
  }
}

class CounterView extends StatefulWidget {
  final int initNumber;
  final Function(int) counterCallback;
  final Function increaseCallback;
  final Function decreaseCallback;
  final int minNumber;
  CounterView(
      {this.initNumber,
      this.counterCallback,
      this.increaseCallback,
      this.decreaseCallback,
      this.minNumber});
  @override
  _CounterViewState createState() => _CounterViewState();
}

class _CounterViewState extends State<CounterView> {
  int _currentCount;
  Function _counterCallback;
  Function _increaseCallback;
  Function _decreaseCallback;
  int _minNumber;

  @override
  void initState() {
    _currentCount = widget.initNumber ?? 1;
    _counterCallback = widget.counterCallback ?? (int number) {};
    _increaseCallback = widget.increaseCallback ?? () {};
    _decreaseCallback = widget.decreaseCallback ?? () {};
    _minNumber = widget.minNumber ?? 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(0),
        color: Color(0xFFEFCEAD),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _createIncrementDicrementButton(Icons.remove, () => _dicrement()),
          Text(
            '$_currentCount',
            style: TextStyle(fontSize: 25),
          ),
          _createIncrementDicrementButton(Icons.add, () => _increment()),
        ],
      ),
    );
  }

  void _increment() {
    setState(() {
      _currentCount++;
      _counterCallback(_currentCount);
      _increaseCallback();
    });
  }

  void _dicrement() {
    setState(() {
      if (_currentCount > 0) {
        _currentCount--;
        _counterCallback(_currentCount);
        _decreaseCallback();
      }
    });
  }

  Widget _createIncrementDicrementButton(IconData icon, Function onPressed) {
    return RawMaterialButton(
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      constraints: BoxConstraints(minWidth: 70.0, minHeight: 32.0),
      onPressed: onPressed,
      elevation: 2.0,
      fillColor: Color(0xFFEFCEAD),
      child: Icon(icon, color: Colors.black, size: 30.0),
      shape: CircleBorder(),
    );
  }
}
