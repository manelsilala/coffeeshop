import 'package:flutter/material.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/widgets/BestcoffeeWidget.dart';
import 'package:flutter_app/widgets/PopularcoffeeWidget.dart';
import 'package:flutter_app/widgets/SearchWidget.dart';
import 'package:flutter_app/widgets/TopMenus.dart';
import 'package:flutter_app/pages/NotificationsPage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF0EAE7),
      appBar: AppBar(
        backgroundColor: Color(0xFFF0EAE7),
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Which taste of coffee do you prefer ? ",
          style: TextStyle(
              color: Color(0xFF3a3a3b),
              fontSize: 18,
              fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xFF3a3737),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.notifications_none,
                color: Color(0xFF3a3737),
              ),
              onPressed: () {
                Navigator.push(context, ScaleRoute(page: NotificationsPage()));
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SearchWidget(),
            TopMenus(),
            PopularcoffeeWidget(),
            BestcoffeeWidget(),
          ],
        ),
      ),
    );
  }
}
