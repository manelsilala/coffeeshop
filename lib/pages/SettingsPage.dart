import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF0EAE7),
      appBar: AppBar(
        backgroundColor: Color(0xFFEFCEAD),
          centerTitle: true,
          title: Text(
            "Settings",
            style: TextStyle(
                color: Colors.brown,
                fontWeight: FontWeight.w600,
                fontSize: 22),
          ),
          leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xFF3a3737),
          ),
        onPressed: () => Navigator.of(context).pop(),
        ),
      ),

      body: SafeArea(
        child:ListView(
          padding: EdgeInsets.all(24),
          children: [
            SizedBox(height:10),
            buildSettiongsOption(context, "Change Password",Icons.lock),
            buildSettiongsOption(context, "Content Settings",Icons.settings),
            buildSettiongsOption(context, "Privacy and Security",Icons.security),
            buildSettiongsOption(context, "Language",Icons.translate),
            buildSettiongsOption(context, "Location",Icons.location_on),
        ],
       ),
      ),
    );
}
GestureDetector buildSettiongsOption(BuildContext context,String title,IconData icon)
{
  return GestureDetector(
    onTap: () { 
      showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: Text (title),
        content: Column(mainAxisSize: MainAxisSize.min,
        children: [
          ],
        ),      
       );  
      });
    },
    child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children:<Widget> [ 
          Icon(icon, color: Color(0xFF3a3737)),
          SizedBox(width: 20),
          Expanded(child: Text(title,
          style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w600, color: Colors.brown))),
          Icon(Icons.arrow_forward_ios, color: Color(0xFF3a3737))
        ],
      ),
    ),
  );
 }
}

