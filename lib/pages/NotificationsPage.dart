import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  bool valNotif1= true;
  onChangeFunction1(bool newValue1){
    setState(() {
      valNotif1=newValue1;
    });
  }
  bool valNotif2= false;
  onChangeFunction2(bool newValue2){
    setState(() {
      valNotif2=newValue2;
    });
  }
  bool valNotif3= false;
  onChangeFunction3(bool newValue3){
    setState(() {
      valNotif3=newValue3;
    });
  }
  bool valNotif4= true;
  onChangeFunction4(bool newValue4){
    setState(() {
      valNotif4=newValue4;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF0EAE7),
      appBar: AppBar(
        backgroundColor: Color(0xFFEFCEAD),
          centerTitle: true,
          title: Text(
            "Notifications",
            style: TextStyle(
                color: Colors.brown,
                fontWeight: FontWeight.w600,
                fontSize: 22),
          ),
          leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xFF3a3737),
          ),
        onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      
      body: SafeArea(
        child:ListView(
          padding: EdgeInsets.all(24),
          children: [
            SizedBox(height:10),
            buildNotificationsOption('News For You', valNotif1, onChangeFunction1),
            buildNotificationsOption('Account Activity', valNotif2, onChangeFunction2),
            buildNotificationsOption('App Updates', valNotif3, onChangeFunction3),
            buildNotificationsOption('Notifications Sound', valNotif4, onChangeFunction4),
          
        ],
       ),
      ),
    );
  }
 Padding buildNotificationsOption(String title, bool value, Function onChangeMethod)
 {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20), 
    child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(title, 
      style: TextStyle(fontSize: 20,
      fontWeight: FontWeight.w600,
      color: Colors.brown,)
      ),
      Transform.scale(
        scale:0.7,
        child: CupertinoSwitch( 
        activeColor: Colors.brown,
        value: value, 
        trackColor: Colors.grey, 
        onChanged: (bool newValue) { 
          onChangeMethod(newValue);
        },
       )
      ),

],
),);
 }
}



