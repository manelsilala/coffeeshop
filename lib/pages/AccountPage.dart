import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/AccountMenu.dart';
import 'package:flutter_app/pages/SettingsPage.dart';
import 'package:flutter_app/pages/NotificationsPage.dart';
import 'package:flutter_app/pages/SignInPage.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class AccountPage extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    String defaultFontFamily = 'Roboto-Bold.ttf';
    double defaultFontSize = 20;

    return Scaffold(  
      body: Container(
        padding: EdgeInsets.only(left: 20, right: 20, top: 100, bottom: 30),
        width: double.infinity,
        height: double.infinity,
        color: Color(0xFFF0EAE7),
        child: Column(
        children: [
          SizedBox(
           height: 130,
           width: 130,
           child : Stack (
             fit: StackFit.expand,
             clipBehavior: Clip.none,
             children:[
             CircleAvatar(backgroundImage: AssetImage("assets/images/menus/person_icon.png"),
              ), 
              Positioned(
                right: -5,
                bottom: 0,
                child: SizedBox(
                 height: 40,
                 width: 40,
                 child: TextButton(
                  style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                    side: BorderSide(color: Colors.white),
                  ),
                  backgroundColor: Color(0xFFF5F6F9),
                  ),
                  onPressed: () {},
                  child : new Icon(
                    FontAwesomeIcons.camera,
                    color: Color(0xFF666666),
                  ),
                   ),
                   ),
                  )
            
          
                ],          
              
              ),
            ),
             SizedBox(height: 10),
             Text(
            'Dorra Jmal',
             style: TextStyle(color: Color(0xFF895B4A),
                              fontFamily: defaultFontFamily,
                              fontSize: defaultFontSize,)),
          
          AccountMenu(
            text: "Notifications",
            icon: LineAwesomeIcons.bell,
            press: () {Navigator.push(context, ScaleRoute(page: NotificationsPage()));},
          ),
          AccountMenu(
            text: "Settings",
            icon:  LineAwesomeIcons.cog,
            press: () {Navigator.push(context, ScaleRoute(page: SettingsPage()));},
          ),
          AccountMenu(
            text: "Help Center",
            icon: LineAwesomeIcons.question_circle,
            press: () => {},
          ),
          AccountMenu(
            text: "Log Out",
            icon: LineAwesomeIcons.alternate_sign_out,
            press: () {Navigator.push(context, ScaleRoute(page: SignInPage()));},
          ),
        
          ],
        ),
      ),
    );
  }
}