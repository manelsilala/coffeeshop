import 'package:flutter/material.dart';
import 'package:flutter_app/pages/CoffeeDetailsPage.dart';
import 'package:flutter_app/pages/CoffeeOrderPagebackend.dart';
import 'package:flutter_app/pages/HomePage.dart';
import 'package:flutter_app/pages/SignInPage.dart';
import 'package:flutter_app/pages/SignUpPage.dart';
import 'package:flutter_app/pages/WelcomePage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app/widgets/productsprovider.dart';
import 'package:flutter_app/widgets/topproductsprovider.dart';
import 'package:flutter_app/widgets/userprovider.dart';
import 'package:flutter_app/widgets/appprovider.dart';
import 'package:flutter_app/widgets/loadingwidget.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: UserProvider.initialize()),
        ChangeNotifierProvider.value(value: ProductsProvider()),
        ChangeNotifierProvider.value(value: TopProductsProvider()),
        ChangeNotifierProvider.value(value: AppProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(fontFamily: 'Alef', hintColor: Color(0xFFd0cece)),
        home: WelcomePage(),
      )));
}

class ScreensController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);

    switch (user.status) {
      case Status.Uninitialized:
        return Loading();
      case Status.Unauthenticated:
      case Status.Authenticating:
        return SignInPage();
      case Status.Authenticated:
        return HomePage();
      default:
        return SignInPage();
    }
  }
}
