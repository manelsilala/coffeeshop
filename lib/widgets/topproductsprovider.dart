import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/topproductmodel.dart';
import 'package:flutter_app/widgets/topproductsservice.dart';

class TopProductsProvider with ChangeNotifier {
  List<TopProducts> productsList = [];
  TopProductsServices _TopproductsServices = TopProductsServices();

  TopProductsProvider() {
    loadProducts();
  }
  Future loadProducts() async {
    productsList = await _TopproductsServices.getProducts();
    notifyListeners();
  }
}
