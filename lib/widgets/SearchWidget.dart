import 'package:flutter/material.dart';

class SearchWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15, top: 5, right: 15, bottom: 15),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(0)),
          ),
          filled: true,
          prefixIcon: Icon(
            Icons.search,
            color: Color(0xFF8B8A8A),
          ),
          fillColor: Color(0xFFF0EAE7),
          hintStyle: new TextStyle(color: Color(0xFF8B8A8A), fontSize: 20),
          hintText: "What would your like to buy?",
        ),
      ),
    );
  }
}
