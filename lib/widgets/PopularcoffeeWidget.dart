import 'package:flutter/material.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/pages/CoffeeDetailsPage.dart';
import 'package:flutter_app/widgets/productsmodal.dart';
import 'package:flutter_app/widgets/productsprovider.dart';
import 'package:provider/provider.dart';

class PopularcoffeeWidget extends StatefulWidget {
  @override
  _PopularcoffeeWidgetState createState() => _PopularcoffeeWidgetState();
}

class _PopularcoffeeWidgetState extends State<PopularcoffeeWidget> {
  @override
  Widget build(BuildContext context) {
    final products = Provider.of<ProductsProvider>(context);
    return Container(
      height: 265,
      width: double.infinity,
      child: Column(children: <Widget>[
        PopularcoffeeTitle(),
        Expanded(
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: products.productsList.length,
                itemBuilder: (_, index) {
                  return PopularcoffeeTiles(
                    name: products.productsList[index].name,
                    imageUrl: products.productsList[index].image,
                    price: products.productsList[index].price,
                    product: products.productsList[index],
                  );
                }))
      ]),
    );
  }
}

@override
class PopularcoffeeTiles extends StatelessWidget {
  final String name;
  final String imageUrl;
  final int price;
  final Products product;

  PopularcoffeeTiles({
    Key key,
    @required this.name,
    @required this.imageUrl,
    @required this.price,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, ScaleRoute(page: CoffeeDetailsPage(product: product)));
      },
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10, right: 5, top: 5, bottom: 5),
            child: Card(
                color: Colors.white,
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                child: Container(
                  width: 170,
                  height: 210,
                  child: Column(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              alignment: Alignment.topRight,
                              width: double.infinity,
                              padding: EdgeInsets.only(right: 5, top: 5),
                              child: Container(
                                height: 28,
                                width: 28,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white70,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xFFfae3e2),
                                        blurRadius: 25.0,
                                        offset: Offset(0.0, 0.75),
                                      ),
                                    ]),
                                child: Icon(
                                  Icons.favorite,
                                  color: Color(0xFFfb3132),
                                  size: 16,
                                ),
                              ),
                            ),
                          ),
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Center(child: Image.network("$imageUrl"))),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.bottomLeft,
                            padding: EdgeInsets.only(left: 10, top: 5),
                            child: Text((name) ?? '0',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500)),
                          ),
                          Container(
                            alignment: Alignment.topRight,
                            padding: EdgeInsets.only(right: 5),
                            child: Container(
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white70,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black,
                                      blurRadius: 25.0,
                                      offset: Offset(0.0, 0.75),
                                    ),
                                  ]),
                              child: Icon(
                                Icons.near_me,
                                color: Color(0xFFfb3132),
                                size: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.topLeft,
                                padding: EdgeInsets.only(left: 5, top: 5),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 3, left: 5),
                              ),
                            ],
                          ),
                          Container(
                            alignment: Alignment.bottomLeft,
                            padding:
                                EdgeInsets.only(left: 0, top: 5, right: 80),
                            child: Text((price.toString()) ?? '0' + 'dt',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600)),
                          )
                        ],
                      )
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }
}

class PopularcoffeeTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "Popluar coffee",
            style: TextStyle(
                fontSize: 20,
                color: Color(0xFF3a3a3b),
                fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }
}
//partie statique
/*class PopularcoffeeItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        PopularcoffeeTiles(
            name: "Marocchino ",
            imageUrl: "marrochin",
            price: 5.000,
            ),
        PopularcoffeeTiles(
            name: "Vienna Coffee",
            imageUrl: "vienna",
            price: 6.000,
            ),
        PopularcoffeeTiles(
            name: "Romano Coffee",
            imageUrl: "romano",
            price: 4.500,),
            
        PopularcoffeeTiles(
            name: "Cappuccino", 
            imageUrl: "cappucin", 
            price: 1.500, 
            
      ],
    );
  }
}*/

