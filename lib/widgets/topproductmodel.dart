import 'package:cloud_firestore/cloud_firestore.dart';

class TopProducts {
  static const IMAGE = "image";
  static const Name = "name";
  static const PRICE = "price";

  String _name;
  String _image;
  int _price;

//   getters
  String get image => _image;
  String get name => _name;
  int get price => _price;

  TopProducts.fromSnapshot(DocumentSnapshot snapshot) {
    _image = snapshot.data[IMAGE];
    _name = snapshot.data[Name];
    _price = snapshot.data[PRICE];
  }
}
