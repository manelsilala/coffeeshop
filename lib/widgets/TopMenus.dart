import 'package:flutter/material.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/widgets/topproductmodel.dart';
import 'package:flutter_app/widgets/topproductsprovider.dart';
import 'package:flutter_app/pages/coffeeDetailsPage2.dart';
import 'package:provider/provider.dart';

class TopMenus extends StatefulWidget {
  @override
  _TopMenusState createState() => _TopMenusState();
}

class _TopMenusState extends State<TopMenus> {
  @override
  Widget build(BuildContext context) {
    final topproducts = Provider.of<TopProductsProvider>(context);
    return Container(
        height: 130,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: topproducts.productsList.length,
            itemBuilder: (_, index) {
              return TopMenuTiles(
                name: topproducts.productsList[index].name,
                imageUrl: topproducts.productsList[index].image,
                product: topproducts.productsList[index],
              );
            }));
  }
}
//partie statique
/*class _TopMenusState extends State<TopMenus> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          TopMenuTiles(name: "Cappuccino", imageUrl: "cappucin", slug: ""),
          TopMenuTiles(name: "Espresso", imageUrl: "espresso", slug: ""),
          TopMenuTiles(name: "Iced Americano", imageUrl: "americano", slug: ""),
          TopMenuTiles(name: "Café Creme", imageUrl: "creme", slug: ""),
          TopMenuTiles(name: "Arabic", imageUrl: "Arabic", slug: ""),
          TopMenuTiles(name: "latte", imageUrl: "latte", slug: ""),
        ],
      ),
    );
  }
}*/

class TopMenuTiles extends StatelessWidget {
  String name;
  String imageUrl;
  TopProducts product;

  TopMenuTiles(
      {Key key,
      @required this.name,
      @required this.imageUrl,
      @required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, ScaleRoute(page: CoffeeDetailsPage2(product: product)));
      },
      child: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 5),
              decoration: new BoxDecoration(boxShadow: []),
              child: Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(3.0),
                  ),
                ),
                child: Container(
                    width: 80,
                    height: 80,
                    child: Center(
                      child: Image.network(
                        "$imageUrl",
                        width: 60,
                        height: 60,
                      ),
                    )),
              )),
          Text((name) ?? '0',
              style: TextStyle(
                  color: Color(0xFF6e6e71),
                  fontSize: 14,
                  fontWeight: FontWeight.w400)),
        ],
      ),
    );
  }
}
