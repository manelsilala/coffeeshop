import 'package:flutter_app/widgets/productsmodal.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProductsServices {
  String collection = "pruducts";
  Firestore _firestore = Firestore.instance;
  Future<List<Products>> getProducts() async =>
      _firestore.collection(collection).getDocuments().then((result) {
        List<Products> productsList = [];
        print("=== RESULT SIZE ${result.documents.length}");
        for (DocumentSnapshot item in result.documents) {
          productsList.add(Products.fromSnapshot(item));
          print(" PRODUCTSSSS ${productsList.length}");
        }
        return productsList;
      });
}
