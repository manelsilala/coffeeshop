import 'package:flutter/material.dart';

class AccountMenu extends StatelessWidget {
  final IconData icon;
  final String text;
  //final bool hasNavigation;
  final VoidCallback press;

  const AccountMenu({
    Key key,
    this.icon,
    this.text,
    //this.hasNavigation = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: TextButton(
        style: TextButton.styleFrom(
          primary: Color(0xFF000000),
          padding: EdgeInsets.all(15),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          backgroundColor: Color(0xFFEFCEAD),
        ),
        //onPressed:(){},
        
        child: Row(
          children: <Widget>[
            Icon(
              this.icon,
              color: Color(0xFF666666),
              size: 22,
            ),
            SizedBox(width: 20),
            Expanded(child: Text(text,style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500, color: Colors.brown))),
            Icon(Icons.arrow_forward_ios, color: Colors.brown),
          ] 
      ),
      onPressed: press,
    ),
  );
  }
}