import 'package:flutter_app/widgets/topproductmodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TopProductsServices {
  String collection = "bestproducts";
  Firestore _firestore = Firestore.instance;
  Future<List<TopProducts>> getProducts() async =>
      _firestore.collection(collection).getDocuments().then((result) {
        List<TopProducts> TopproductsList = [];
        print("=== RESULT SIZE ${result.documents.length}");
        for (DocumentSnapshot item in result.documents) {
          TopproductsList.add(TopProducts.fromSnapshot(item));
          print(" PRODUCTSSSS ${TopproductsList.length}");
        }
        return TopproductsList;
      });
}
