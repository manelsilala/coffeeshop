import 'package:flutter/material.dart';

class BestcoffeeWidget extends StatefulWidget {
  @override
  _BestcoffeeWidgetState createState() => _BestcoffeeWidgetState();
}

class _BestcoffeeWidgetState extends State<BestcoffeeWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      width: double.infinity,
      child: Column(
        children: <Widget>[
          BestcoffeeTitle(),
          Expanded(
            child: BestcoffeeList(),
          )
        ],
      ),
    );
  }
}

class BestcoffeeTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "Best coffees",
            style: TextStyle(
                fontSize: 20,
                color: Color(0xFF3a3a3b),
                fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }
}

class BestcoffeeTiles extends StatelessWidget {
  String name;
  String imageUrl;

  String price;
  String slug;

  BestcoffeeTiles(
      {Key key,
      @required this.name,
      @required this.imageUrl,
      @required this.price,
      @required this.slug})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10, right: 5, top: 5, bottom: 5),
            decoration: BoxDecoration(boxShadow: [
              /* BoxShadow(
                color: Color(0xFFfae3e2),
                blurRadius: 15.0,
                offset: Offset(0, 0.75),
              ),*/
            ]),
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Image.asset(
                'assets/images/bestcoffee/' + imageUrl + ".jpg",
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 1,
              margin: EdgeInsets.all(5),
            ),
          ),
        ],
      ),
    );
  }
}

class BestcoffeeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        BestcoffeeTiles(
            name: "brunch", imageUrl: "best_3", price: '2.500', slug: ""),
        BestcoffeeTiles(
            name: "morning", imageUrl: "best_2", price: "5.000", slug: ""),
        BestcoffeeTiles(
            name: "friends", imageUrl: "best_1", price: "11.00", slug: ""),
      ],
    );
  }
}
