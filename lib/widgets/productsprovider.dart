import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/productsmodal.dart';
import 'package:flutter_app/widgets/pruductsservice.dart';
import 'package:flutter_app/widgets/productsprovider.dart';

class ProductsProvider with ChangeNotifier {
  List<Products> productsList = [];
  ProductsServices _productsServices = ProductsServices();

  ProductsProvider() {
    loadProducts();
  }
  Future loadProducts() async {
    productsList = await _productsServices.getProducts();
    notifyListeners();
  }
}
