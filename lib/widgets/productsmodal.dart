import 'package:cloud_firestore/cloud_firestore.dart';

class Products {
  static const IMAGE = "image";
  static const Name = "name";
  static const PRICE = "price";

  String _name;
  int _price;
  String _image;

//   getters
  String get image => _image;
  String get name => _name;
  int get price => _price;

  Products.fromSnapshot(DocumentSnapshot snapshot) {
    _image = snapshot.data[IMAGE];
    _name = snapshot.data[Name];
    _price = snapshot.data[PRICE];
  }
}
